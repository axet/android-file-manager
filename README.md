# File Manager

_File Manager with two panels and root support._

Smart copy/move dialogs, easy open/share files, multiple files sources including: root, SAF and direct files access.

Android friendly!

# Manual install

    gradle installDebug

# Translate

If you want to translate 'File Manager' to your language  please read this:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

# Screenshots

![shot](/metadata/screenshots/shot.png)
