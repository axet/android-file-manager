package com.github.axet.filemanager.widgets;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.provider.DocumentFile;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.MainApplication;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.filemanager.R;
import com.github.axet.filemanager.app.FilesApplication;
import com.github.axet.filemanager.app.Storage;
import com.github.axet.filemanager.fragments.FilesFragment;

import java.io.File;
import java.util.ArrayList;

public class OpenFileDialog extends com.github.axet.androidlibrary.widgets.OpenFileDialog {
    public static final String TAG = OpenFileDialog.class.getSimpleName();

    protected String title;
    protected Uri currentPath; // can points to file (highlight)
    protected StorageAdapter storage;
    protected FilesFragment.Adapter adapter;
    protected int selectedIndex;

    public static void setDrawable(TextView view, Drawable drawable, int dp) {
        if (view != null) {
            if (drawable != null) {
                int size = ThemeUtils.dp2px(view.getContext(), dp);
                drawable.setBounds(0, 0, size, size);
                view.setCompoundDrawables(drawable, null, null, null);
            } else {
                view.setCompoundDrawables(null, null, null, null);
            }
        }
    }

    public static Drawable getDrawable(Context context, int id, int tint) {
        Drawable d = ContextCompat.getDrawable(context, id);
        d = DrawableCompat.wrap(d);
        d.mutate();
        DrawableCompat.setTint(d, ThemeUtils.getThemeColor(context, tint));
        return d;
    }

    public static class StorageAdapter extends com.github.axet.androidlibrary.widgets.OpenFileDialog.StorageAdapter {
        public ArrayList<Storage.Node> nodes = new ArrayList<>();

        public StorageAdapter(Context context, boolean readonly) {
            super(context, readonly);
            for (File f : list)
                nodes.add(new Storage.Node(f));
            FilesApplication app = FilesApplication.from(context);
            for (Uri u : app.bookmarks)
                add(u);
        }

        public void add(Uri u) {
            for (Storage.Node n : nodes) { // TODO better find dupes
                if (n.uri.equals(u))
                    return;
            }
            String s = u.getScheme();
            if (Build.VERSION.SDK_INT >= 21 && s.equals(ContentResolver.SCHEME_CONTENT))
                nodes.add(new Storage.Node(Storage.getDocumentFile(context, u)));
            else if (s.equals(ContentResolver.SCHEME_FILE))
                nodes.add(new Storage.Node(Storage.getFile(u)));
            else
                throw new Storage.UnknownUri();
        }

        public int find(Uri uri) { // TODO sort by length first
            for (int i = 0; i < nodes.size(); i++) {
                Storage.Node f = nodes.get(i);
                if (uri.getPath().startsWith(f.uri.getPath()))
                    return i;
            }
            return -1;
        }

        @Override
        public int getCount() {
            return nodes.size();
        }

        @Override
        public Object getItem(int position) {
            return nodes.get(position);
        }

        @Override
        public void updateView(View convertView, int position) {
            Storage.Node f = nodes.get(position);
            TextView title = (TextView) convertView.findViewWithTag("text");
            title.setText(Storage.getDisplayName(context, f.uri));
            TextView free = (TextView) convertView.findViewWithTag("free");
            free.setText(MainApplication.formatSize(context, com.github.axet.androidlibrary.app.Storage.getFree(context, f.uri)));
        }
    }

    public OpenFileDialog(Context context, DIALOG_TYPE type) {
        super(context, type);
        currentPath = Uri.fromFile(Environment.getExternalStorageDirectory());
    }

    public OpenFileDialog(Context context, DIALOG_TYPE type, boolean readonly) {
        this(context, type);
        this.readonly = readonly;
        storage = new StorageAdapter(context, readonly);
    }

    @Override
    public AlertDialog create() {
        final AlertDialog d = super.create();
        toolbarText.setPadding(0, 0, 0, 0);
        setDrawable(toolbarText, getDrawable(getContext(), R.drawable.ic_folder, R.attr.colorAccent), 48);
        setAdapter((FileAdapter) null);
        if (adapter == null) {
            adapter = new FilesFragment.Adapter(getContext()) {
                @Override
                public void onBindViewHolder(FilesFragment.Holder h, int position) {
                    super.onBindViewHolder(h, position);
                    OpenFileDialog.this.onBindViewHolder(h, position);
                }

                @Override
                public void onItemClick(FilesFragment.Holder h, Storage.Node f, boolean dir) {
                    OpenFileDialog.this.onItemClick(h, f, dir);
                }

                @Override
                public void onLongItemClick(FilesFragment.Holder h, Storage.Node f, boolean dir) {
                    OpenFileDialog.this.onLongItemClick(h, f, dir);
                }
            };
        }
        setAdapter(adapter);
        toolbarText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = currentPath.getScheme();
                if (s.equals(ContentResolver.SCHEME_FILE)) {
                    File parentDirectory = Storage.getFile(currentPath);
                    File old = parentDirectory;
                    if (CACHE.isDirectory(parentDirectory) || !parentDirectory.exists()) { // allow virtual up
                        parentDirectory = parentDirectory.getParentFile();
                    } else {
                        parentDirectory = parentDirectory.getParentFile();
                        if (parentDirectory != null)
                            parentDirectory = parentDirectory.getParentFile();
                    }

                    if (parentDirectory == null)
                        parentDirectory = old;
                    currentPath = Uri.fromFile(parentDirectory);
                } else {
                    Uri p = Storage.getParent(getContext(), currentPath);
                    if (p == null)
                        return;
                    currentPath = p;
                }
                rebuildFiles();
            }
        });
        titlebar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setSingleChoiceItems(storage, storage.find(currentPath), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        currentPath = storage.nodes.get(which).uri;
                        rebuildFiles();
                        dialog.dismiss();
                    }
                });
                AlertDialog d = builder.create();
                d.show();
            }
        });
        if (newFolderButton != null) {
            newFolderButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final EditTextDialog builder = new EditTextDialog(getContext());
                    builder.setTitle(com.github.axet.androidlibrary.R.string.filedialog_foldername);
                    builder.setText("");
                    builder.setPositiveButton(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Uri uri = Storage.mkdir(getContext(), currentPath, builder.getText());
                            if (uri == null)
                                toast(getContext().getString(R.string.filedialog_unablecreatefolder, builder.getText()));
                            else
                                toast(getContext().getString(R.string.filedialog_foldercreated, builder.getText()));
                            rebuildFiles();
                        }
                    });
                    builder.show();
                }
            });
        }
        return d;
    }

    @Override
    protected void updateSelected(int i) {
        if (positive != null) {
            switch (type) {
                case FILE_DIALOG:
                    positive.setEnabled(i != -1);
                    break;
                default:
                    positive.setEnabled(true);
            }
        }
        selectedIndex = i;
    }

    @Override
    public void rebuildFiles() {
        adapter.load(currentPath);

        listView.scrollToPosition(0);
        path.setText(Storage.getDisplayName(getContext(), currentPath));
        free.setText(MainApplication.formatSize(getContext(), Storage.getFree(getContext(), currentPath)));
        if (changeFolder != null)
            changeFolder.run();

        String s = currentPath.getScheme();
        if (s.equals(ContentResolver.SCHEME_FILE)) {
            if (!readonly) { // show readonly directory tooltip
                File p = Storage.getFile(currentPath);
                while (!p.exists())
                    p = p.getParentFile();
                if (!p.canWrite()) {
                    message.setText(R.string.filedialog_readonly);
                    message.setVisibility(View.VISIBLE);
                } else {
                    message.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    protected void onChanged() {
        if (Storage.exists(getContext(), currentPath) && !Storage.isDirectory(getContext(), currentPath))  // file or symlink
            updateSelected(adapter.find(currentPath));
        else
            updateSelected(-1);
    }

    public void setAdapter(FilesFragment.Adapter a) {
        if (adapter != null)
            adapter.unregisterAdapterDataObserver(observer);
        adapter = a;
        if (listView != null)
            listView.setAdapter(a);
        if (a != null)
            a.registerAdapterDataObserver(observer);
    }

    public Uri getUriPath() {
        return currentPath;
    }

    @Override
    public void scrollToSelection() {
        listView.scrollToPosition(selectedIndex);
    }

    public void onItemClick(FilesFragment.Holder h, Storage.Node f, boolean dir) {
        if (dir) {
            currentPath = f.uri;
            rebuildFiles();
        } else {
            int index = adapter.files.indexOf(f);
            switch (type) {
                case FILE_DIALOG:
                case BOOTH:
                    if (index != selectedIndex) {
                        updateSelected(index);
                    } else {
                        currentPath = Storage.getParent(getContext(), f.uri);
                        updateSelected(-1);
                    }
                    adapter.notifyDataSetChanged();
                    break;
                default:
                    Toast.makeText(getContext(), R.string.filedialog_selectfolder, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onLongItemClick(FilesFragment.Holder h, Storage.Node f, boolean dir) {
        final PopupMenu p = new PopupMenu(getContext(), h.itemView);
        if (!readonly) { // show rename / delete
            p.getMenu().add(getContext().getString(com.github.axet.androidlibrary.R.string.filedialog_rename));
            p.getMenu().add(getContext().getString(com.github.axet.androidlibrary.R.string.filedialog_delete));
        }
        p.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals(getContext().getString(com.github.axet.androidlibrary.R.string.filedialog_rename))) {
                    final EditTextDialog b = new EditTextDialog(getContext());
                    b.setTitle(getContext().getString(com.github.axet.androidlibrary.R.string.filedialog_foldername));
                    b.setText(f.name);
                    b.setPositiveButton(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Uri uri = Storage.rename(getContext(), f.uri, b.getText());
                            if (uri == null)
                                toast(getContext().getString(R.string.not_permitted));
                            else
                                toast(getContext().getString(com.github.axet.androidlibrary.R.string.filedialog_renameto, f.name));
                            rebuildFiles();
                        }
                    });
                    b.show();
                    return true;
                }
                if (item.getTitle().equals(getContext().getString(com.github.axet.androidlibrary.R.string.filedialog_delete))) {
                    if (Storage.delete(getContext(), f.uri))
                        toast(getContext().getString(com.github.axet.androidlibrary.R.string.filedialog_folderdeleted, f.name));
                    else
                        toast(getContext().getString(R.string.not_permitted));
                    rebuildFiles();
                    return true;
                }
                return false;
            }
        });

        if (p.getMenu().size() != 0)
            p.show();
    }

    public void onBindViewHolder(FilesFragment.Holder h, int position) {
        h.circle.setVisibility(View.GONE);
        h.selected.setVisibility(View.GONE);
        h.unselected.setVisibility(View.GONE);
    }
}
