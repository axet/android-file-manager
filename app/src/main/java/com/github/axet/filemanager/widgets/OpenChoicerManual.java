package com.github.axet.filemanager.widgets;

import android.content.DialogInterface;

import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.filemanager.R;

import java.io.File;

public class OpenChoicerManual extends com.github.axet.androidlibrary.widgets.OpenChoicer {
    public OpenChoicerManual(OpenFileDialog.DIALOG_TYPE type, boolean readonly) {
        super(type, readonly);
    }

    @Override
    public OpenFileDialog fileDialogBuild() {
        OpenFileDialog d = super.fileDialogBuild();
        d.setNeutralButton(R.string.manual_path, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                OpenFileDialog.EditTextDialog edit = manualDialogBuild(d, dialog);
                edit.show();
            }
        });
        return d;
    }

    public OpenFileDialog.EditTextDialog manualDialogBuild(OpenFileDialog d, DialogInterface dialog) {
        final OpenFileDialog.EditTextDialog edit = new OpenFileDialog.EditTextDialog(context);
        edit.setTitle(R.string.legacy_title);
        File p = d.getCurrentPath();
        edit.setText(p.getAbsolutePath());
        edit.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog2, int which) {
                String s = edit.getText();
                if (!s.isEmpty())
                    d.setCurrentPath(new File(s));
            }
        });
        return edit;
    }
}
