package com.github.axet.filemanager.activities;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.github.axet.androidlibrary.activities.AppCompatThemeActivity;
import com.github.axet.androidlibrary.preferences.ScreenlockPreference;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.OpenChoicer;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.filemanager.R;
import com.github.axet.filemanager.app.FilesApplication;
import com.github.axet.filemanager.app.Storage;
import com.github.axet.filemanager.fragments.FilesFragment;
import com.github.axet.filemanager.widgets.OpenChoicerManual;
import com.github.axet.filemanager.widgets.OpenFileDialog;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class ShareActivity extends AppCompatThemeActivity {

    public static final int RESULT_SHARE = 1;

    Storage storage;
    OpenChoicerManual choicer;

    public static String getName(String str) {
        Uri u = Uri.parse(str);
        String text = u.getQuery();
        if (text == null || text.isEmpty()) {
            text = u.getEncodedPath();
            if (text == null || text.isEmpty())
                text = u.getAuthority();
            if (text == null || text.isEmpty())
                text = u.getScheme();
            if (text == null || text.isEmpty())
                text = str;
        }
        return new File(text).getName();
    }

    @Override
    public int getAppTheme() {
        return FilesApplication.getTheme(this, R.style.AppThemeDialogLight, R.style.AppThemeDialogDark);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ScreenlockPreference.isLocked(this))
            showLocked(getWindow());

        storage = new Storage(this);

        Intent intent = getIntent();
        String a = intent.getAction();
        Bundle extras = intent.getExtras();
        final ArrayList<Uri> uu;

        if (a.equals(Intent.ACTION_SEND)) {
            Uri uri = extras.getParcelable(Intent.EXTRA_STREAM);
            if (uri == null)
                uri = intent.getData();
            if (uri != null) {
                uu = new ArrayList<>();
                uu.add(uri);
            } else {
                uu = null;
            }
        } else if (a.equals(Intent.ACTION_SEND_MULTIPLE)) {
            uu = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        } else {
            uu = null;
        }

        String text = extras.getString(Intent.EXTRA_TEXT);
        String name1 = extras.getString(Intent.EXTRA_SUBJECT);
        if (name1 == null || name1.isEmpty()) {
            if (uu != null) {
                name1 = Storage.getName(this, uu.get(0));
            } else {
                if (text == null || text.isEmpty()) {
                    Toast.makeText(this, "Empty intent", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                } else {
                    name1 = getName(text);
                }
            }
        }
        String name = name1;

        choicer = new OpenChoicerManual(OpenFileDialog.DIALOG_TYPE.FOLDER_DIALOG, false) {
            @Override
            public OpenFileDialog fileDialogBuild() {
                final OpenChoicer that = this;
                final OpenFileDialog dialog = new OpenFileDialog(context, type, readonly);
                if (old != null) {
                    String s = old.getScheme();
                    if (s.equals(ContentResolver.SCHEME_FILE))
                        dialog.setCurrentPath(Storage.getFile(old));
                }
                dialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface d, int which) {
                        Uri f = dialog.getUriPath();
                        if (f == null)
                            onResult(null, false);
                        else
                            onResult(f, false);
                    }
                });
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        that.onCancel();
                    }
                });
                dialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        that.onCancel();
                    }
                });
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface d) {
                        that.onDismiss();
                    }
                });
                dialog.setAdapter(new FilesFragment.Adapter(context) {
                    @Override
                    public void onBindViewHolder(FilesFragment.Holder h, int position) {
                        super.onBindViewHolder(h, position);
                        dialog.onBindViewHolder(h, position);
                    }

                    @Override
                    public void onItemClick(FilesFragment.Holder h, Storage.Node f, boolean dir) {
                        dialog.onItemClick(h, f, dir);
                    }

                    @Override
                    public void onLongItemClick(FilesFragment.Holder h, Storage.Node f, boolean dir) {
                    }
                });
                return dialog;
            }

            @Override
            public void onResult(Uri uriTo) {
                String to = Storage.getNextName(context, uriTo, Storage.getNameNoExt(name), Storage.getExt(name));
                try {
                    ContentResolver resolver = context.getContentResolver();
                    if (uu != null) {
                        for (Uri u : uu) {
                            Storage.UriOutputStream os = storage.open(uriTo, to);
                            InputStream is = resolver.openInputStream(u);
                            IOUtils.copy(is, os.os);
                            if (is != null) {
                                try {
                                    is.close();
                                } catch (Exception e) {
                                    Log.w(TAG, e);
                                }
                            }
                            if (os.os != null) {
                                try {
                                    os.os.close();
                                } catch (Exception e) {
                                    Log.w(TAG, e);
                                }
                            }
                            Toast.makeText(context, Storage.getDisplayName(context, os.uri), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Storage.UriOutputStream os = storage.open(uriTo, to);
                        IOUtils.write(text, os.os, Charset.defaultCharset());
                        if (os.os != null) {
                            try {
                                os.os.close();
                            } catch (Exception e) {
                                Log.w(TAG, e);
                            }
                        }
                        Toast.makeText(context, Storage.getDisplayName(context, os.uri), Toast.LENGTH_LONG).show();
                    }
                } catch (IOException e) {
                    Toast.makeText(context, ErrorDialog.toMessage(e), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onDismiss() {
                finish();
            }
        };
        choicer.setTitle(getString(R.string.save_to));
        choicer.setPermissionsDialog(this, Storage.PERMISSIONS_RW, RESULT_SHARE);
        if (Build.VERSION.SDK_INT >= 21)
            choicer.setStorageAccessFramework(this, RESULT_SHARE);
        choicer.show(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setVisible(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SHARE:
                choicer.onActivityResult(resultCode, data);
                break;
        }
    }
}
